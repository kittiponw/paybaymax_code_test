trait Queue[T] {
  def isEmpty: Boolean
  def enQueue(t: T): Queue[T]
  def deQueue(): Queue[T]
  def head: Option[T]
}

object Queue {
  def empty[T]: Queue[T] = new QueueImpl[T](List[T]())

  private class QueueImpl[T](list: List[T]) extends Queue[T] {
    override def isEmpty: Boolean = list.isEmpty
    override def enQueue(t: T): Queue[T] = new QueueImpl[T](list :+ t)
    override def deQueue(): Queue[T] = list match {
      case n :: rest => new QueueImpl[T](rest)
      case _ => throw new NoSuchElementException("deQueue on empty queue")
    }
    override def head: Option[T] = {
      try { 
        Some(list.head) 
      } catch {
        case e: Exception => None
      }
    }
  }
}