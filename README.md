This project use `sbt` as a build tool for scala

### Prerequisite:
  * Install Scala
  * Install SBT

### How to run:
  * run `sbt test` to see unit testing result