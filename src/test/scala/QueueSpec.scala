import org.scalatest.{Matchers, WordSpec}

class QueueSpec extends WordSpec with Matchers {
  "Queue" should {
    "empty[T] to create an empty list" in {
      val queue = Queue.empty[Int]

      queue.isEmpty shouldBe true
    }

    "enQueue(t: T) to create a new queue with element t added to the end" in {
      val q1 = Queue.empty[String]
      val q2 = q1.enQueue("A")

      q1.isEmpty shouldBe true
      q1.head shouldBe None

      q2.isEmpty shouldBe false
      q2.head shouldBe Some("A")
    }

    "head() to return the first element in the queue" in {
      val q1 = Queue.empty[String]
      val q2 = q1.enQueue("AA")
      val q3 = q2.enQueue("BB")
      val q4 = q3.enQueue("CC")

      q4.head shouldBe Some("AA")
    }

    "deQueue() to remove begining element and return a new queue" in {
      val q1 = Queue.empty[Int]
      val q2 = q1.enQueue(1)

      val q3 = q2.enQueue(2)
      q3.head shouldBe Some(1)

      val q4 = q3.deQueue
      q4.head shouldBe Some(2)
    }

    "throw NoSuchElementException" when {
      "deQueue() on an empty list" in {
        val q1 = Queue.empty[Int]
        a [NoSuchElementException] should be thrownBy q1.deQueue
      }
    }
  }
}